/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_ADDDATAGLOVEHANDLERDIALOG_H_
#define __MMM_ADDDATAGLOVEHANDLERDIALOG_H_

#include <QDialog>
#include <QSettings>

#ifndef Q_MOC_RUN
#include "../DataGloveConverter.h"
#include <MMM/Motion/Motion.h>
#endif

namespace Ui {
class AddDataGloveHandlerDialog;
}

class AddDataGloveHandlerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddDataGloveHandlerDialog(QWidget* parent, MMM::MotionList motions);
    ~AddDataGloveHandlerDialog();

    bool convertMotion();

private slots:
    void setCurrentMotion(int index);
    void chooseMotionToggled(bool checked);
    void loadConfiguration();
    void loadData();
    void convert();

private:
    void loadMotions();
    void loadConfiguration(const std::string &converterConfigFilePath);
    void setConfigLabel(const std::string &filePath);
    void loadData(const std::string &converterDataFilePath);
    void setDataLabel(const std::string &filePath);
    void enableConvertButton();

    Ui::AddDataGloveHandlerDialog* ui;
    MMM::MotionList motions;
    MMM::MotionPtr currentMotion;
    MMM::DataGloveConverterPtr converter;
    std::string converterDataFilePath;
    bool motionConverted;
    QSettings settings;
};

#endif // __MMM_AddDataGloveHandlerDIALOG_H_
