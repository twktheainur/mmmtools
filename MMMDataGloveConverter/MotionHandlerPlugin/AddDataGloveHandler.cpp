#include "AddDataGloveHandler.h"
#include "AddDataGloveHandlerDialog.h"
#include <QFileDialog>
#include <QMessageBox>

using namespace MMM;

AddDataGloveHandler::AddDataGloveHandler(QWidget* widget) :
    MotionHandler(MotionHandlerType::ADD_SENSOR, "Add kinematic sensor from data glove"),
    searchPath(std::string(MMMTools_SRC_DIR)),
    widget(widget)
{
}

void AddDataGloveHandler::handleMotion(MotionList motions) {
    if (motions.size() > 0) {
        MMM::MotionList convertableMotions;
        for (MMM::MotionPtr motion : motions) {
            if (motion->getModel()) convertableMotions.push_back(motion);
            else MMM_INFO << "Ignoring motion with name " + motion->getName() + ", because is does not have a model!";
        }
        if (convertableMotions.size() == 0) {
            QMessageBox* msgBox = new QMessageBox(widget);
            msgBox->setText("No motions found for adding data glove data!");
            msgBox->exec();
        } else {
            AddDataGloveHandlerDialog* dialog = new AddDataGloveHandlerDialog(widget, convertableMotions);
            if (dialog->convertMotion()) emit openMotions(motions);
        }
    }
    else MMM_ERROR << "Cannot open add data glove dialog, because no motions are present!" << std::endl;
}

std::string AddDataGloveHandler::getName() {
    return NAME;
}
