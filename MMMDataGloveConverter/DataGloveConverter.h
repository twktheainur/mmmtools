/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2018 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_DATAGLOVECONVERTER_H_
#define __MMM_DATAGLOVECONVERTER_H_

#include <boost/shared_ptr.hpp>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>

namespace MMM
{

class FingerMapping;

typedef boost::shared_ptr<FingerMapping> FingerMappingPtr;
typedef std::vector<FingerMappingPtr> FingerMappingList;

class FingerMapping
{
public:
    FingerMapping(unsigned int index, const std::string &jointName, const std::string &description = std::string(), float multiplyBy = 1.0f);

    static std::vector<std::string> getJointNames(const FingerMappingList &mappings);

    float getJointAngle(const std::vector<std::string> &values, int row);

private:
    unsigned int index;
    std::string jointName;
    std::string description;
    float multiplyBy;
};

class DataGloveConverter
{

public:
    /** @throws MMMException */
    DataGloveConverter(const std::string &dataGloveConfigPath);

    /** @throws MMMException */
    KinematicSensorPtr convert(const std::string &dataGloveDataPath);

private:
    void loadConfig(const std::string &dataGloveConfigPath);

    std::string separator;
    bool hasHeader;
    unsigned int timestepIndex;
    bool useTimestepIndex;
    float timestepDelta;
    FingerMappingList fingerMapping;
};

typedef boost::shared_ptr<DataGloveConverter> DataGloveConverterPtr;

}

#endif
