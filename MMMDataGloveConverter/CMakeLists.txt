cmake_minimum_required(VERSION 3.10.2)

###########################################################
#### Project configuration                             ####
###########################################################

project(MMMDataGloveConverter)

###########################################################
#### Source configuration                              ####
###########################################################

set(SOURCE_FILES
    MMMDataGloveConverter.cpp
    DataGloveConverter.cpp
    MMMDataGloveConverterConfiguration.cpp
    ../common/ApplicationBaseConfiguration.cpp
)

set(HEADER_FILES
    MMMDataGloveConverterConfiguration.h
    DataGloveConverter.h
    DummyMotion.h
    ../common/ApplicationBaseConfiguration.h
)

###########################################################
#### CMake package configuration                       ####
###########################################################

find_package(MMMCore REQUIRED)
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} MMMCore KinematicSensor ModelPoseSensor)

find_package(Simox REQUIRED)
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} VirtualRobot)

set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} MMMSimoxTools)

###########################################################
#### Project build configuration                       ####
###########################################################

add_executable(${PROJECT_NAME} ${SOURCE_FILES} ${HEADER_FILES})
target_link_libraries(${PROJECT_NAME} ${EXTERNAL_LIBRARY_DIRS} dl)

set(MMMDataGloveConverter_BASE_DIR ${PROJECT_SOURCE_DIR} CACHE INTERNAL "")
add_definitions(-DMMMDataGloveConverter_BASE_DIR="${MMMDataGloveConverter_BASE_DIR}" -D_SCL_SECURE_NO_WARNINGS)

install(
    TARGETS ${PROJECT_NAME}
    EXPORT "${CMAKE_PROJECT_NAME}Targets"
    #LIBRARY DESTINATION lib
    #ARCHIVE DESTINATION lib
    RUNTIME DESTINATION bin
    #INCLUDES DESTINATION include
    COMPONENT bin
)

add_definitions(-DMMMTools_LIB_DIR="${MMMTools_LIB_DIR}")
add_definitions(-DMMMTools_SRC_DIR="${MMMTools_SOURCE_DIR}")

###########################################################
#### Compiler configuration                            ####
###########################################################

include_directories(${EXTERNAL_INCLUDE_DIRS})
link_directories(${EXTERNAL_LIBRARY_DIRS})
add_definitions(${EXTERNAL_LIBRARY_FLAGS})

###########################################################
#### Motion Handler Plugin                             ####
###########################################################

add_subdirectory(MotionHandlerPlugin)
