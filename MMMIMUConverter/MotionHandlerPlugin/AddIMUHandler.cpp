#include "AddIMUHandler.h"
#include "../AddIMUHandlerSensorConfiguration.h"

#include <QFileDialog>
#include <QMessageBox>

using namespace MMM;

AddIMUHandler::AddIMUHandler(QWidget* parent) :
    MotionHandler(MotionHandlerType::ADD_SENSOR, "Add IMU sensor"),
    dialog(0),
    widget(parent)
{
}

void AddIMUHandler::handleMotion(MotionList motions) {
    if (!dialog) dialog = new AddAttachedSensorDialog(AddIMUHandlerSensorConfigurationPtr(new AddIMUHandlerSensorConfiguration()), widget);
    if (motions.size() > 0) {
        MMM::MotionList calculatableMotions;
        for (MMM::MotionPtr motion : motions) {
            if (!motion->getModel()) {
                MMM_INFO << "Ignoring motion '" + motion->getName() + "'! Because the motion has no model." << std::endl;
                continue;
            }
            calculatableMotions.push_back(motion);
        }
        if (calculatableMotions.size() == 0) {
            QMessageBox* msgBox = new QMessageBox(widget);
            msgBox->setText("No calculatable motions found!");
            msgBox->exec();
        } else {
            if (dialog->addSensor(calculatableMotions)) emit openMotions(motions);
        }
    }
    else MMM_ERROR << "Cannot open add imu sensor dialog, because no motions are present!" << std::endl;
}

std::string AddIMUHandler::getName() {
    return NAME;
}
