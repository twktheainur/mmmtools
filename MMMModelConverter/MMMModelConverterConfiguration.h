/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2018 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MODELCONVERTERCONFIGURATION_H_
#define __MMM_MODELCONVERTERCONFIGURATION_H_

#include <boost/filesystem.hpp>
#include <string>
#include <VirtualRobot/RuntimeEnvironment.h>
#include <MMM/MMMCore.h>
#include <MMM/XMLTools.h>

#include "../common/ApplicationBaseConfiguration.h"

/*!
    Configuration of MMMModelConverter.
*/
class MMMModelConverterConfiguration : public ApplicationBaseConfiguration
{
public:
    MMMModelConverterConfiguration() : ApplicationBaseConfiguration()
    {
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("inputModel");
        VirtualRobot::RuntimeEnvironment::considerKey("motionName");
        VirtualRobot::RuntimeEnvironment::considerKey("outputMotion");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        inputModelPath = getParameter("inputMotion", true, true);
        motionName = getParameter("motionName");
        if (motionName.empty())
            motionName = MMM::XML::getFileNameWithoutExtension(inputModelPath);
        outputMotionPath = getParameter("outputMotion", false, false);
        if (outputMotionPath.empty())
            outputMotionPath = MMM::XML::getFileNameWithoutExtension(inputModelPath) + "_as_motion.xml";


        return valid;
    }

    void print()
    {
        MMM_INFO << "*** MMMModelsConverter Configuration ***" << std::endl;
        std::cout << "Input model: " << inputModelPath << std::endl;
        std::cout << "Output motion: " << outputMotionPath << std::endl;
    }

    std::string inputModelPath;
    std::string motionName;
    std::string outputMotionPath;
};


#endif // __MMM_MODELCONVERTERCONFIGURATION_H_
