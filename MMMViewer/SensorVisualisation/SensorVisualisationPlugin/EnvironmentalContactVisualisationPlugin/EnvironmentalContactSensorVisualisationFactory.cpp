#include "EnvironmentalContactSensorVisualisationFactory.h"
#include "EnvironmentalContactSensorVisualisation.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/EnvironmentalContactPlugin/EnvironmentalContactSensor.h>

#include <boost/shared_ptr.hpp>
#include <memory>

using namespace MMM;

// register this factory
SensorVisualisationFactory::SubClassRegistry EnvironmentalContactSensorVisualisationFactory::registry(VIS_STR(EnvironmentalContactSensor::TYPE), &EnvironmentalContactSensorVisualisationFactory::createInstance);

EnvironmentalContactSensorVisualisationFactory::EnvironmentalContactSensorVisualisationFactory() : SensorVisualisationFactory() {}

EnvironmentalContactSensorVisualisationFactory::~EnvironmentalContactSensorVisualisationFactory() = default;

std::string EnvironmentalContactSensorVisualisationFactory::getName()
{
    return VIS_STR(EnvironmentalContactSensor::TYPE);
}

SensorVisualisationPtr EnvironmentalContactSensorVisualisationFactory::createSensorVisualisation(SensorPtr sensor, VirtualRobot::RobotPtr robot, VirtualRobot::CoinVisualizationPtr visualization, SoSeparator* sceneSep) {
    using boost::dynamic_pointer_cast;
    using std::dynamic_pointer_cast;
    EnvironmentalContactSensorPtr s = dynamic_pointer_cast<EnvironmentalContactSensor>(sensor);
    if (!s) {
        MMM_ERROR << sensor->getType() << " could not be castet to " << EnvironmentalContactSensor::TYPE << std::endl;
        return nullptr;
    }
    return SensorVisualisationPtr(new EnvironmentalContactSensorVisualisation(s, robot, visualization, sceneSep));
}

SensorVisualisationFactoryPtr EnvironmentalContactSensorVisualisationFactory::createInstance(void *)
{
    return SensorVisualisationFactoryPtr(new EnvironmentalContactSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorVisualisationFactoryPtr getFactory() {
    return SensorVisualisationFactoryPtr(new EnvironmentalContactSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorVisualisationFactory::VERSION;
}

