/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_PLUGINHANDLERDIALOG_H_
#define __MMM_PLUGINHANDLERDIALOG_H_

#include <QDialog>
#include <QTreeWidget>

#ifndef Q_MOC_RUN
#include "PluginHandler.h"
#endif

namespace Ui {

class PluginHandlerDialog;
}

class PluginHandlerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PluginHandlerDialog(QWidget *parent = 0);
    ~PluginHandlerDialog();

    void addPluginHandler(boost::shared_ptr<IPluginHandler> pluginHandler);
    void removePluginHandler(const std::string &name);

private slots:
    void addPluginDirectory();
    void pluginTabChanged(int i);
    void change(QTreeWidgetItem* item);

private:
    QTreeWidget* createPluginHandlerWidget(boost::shared_ptr<IPluginHandler> pluginHandler);
    boost::shared_ptr<IPluginHandler> getCurrentPluginHandler();
    int containsPluginHandler(boost::shared_ptr<IPluginHandler> pluginHandler);
    int containsPluginHandler(std::string name);

    Ui::PluginHandlerDialog *ui;
    int currentPluginHandler;
    std::vector<boost::shared_ptr<IPluginHandler> > pluginHandlerList;
};

#endif // __MMM_PLUGINHANDLERDIALOG_H_
