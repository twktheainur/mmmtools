/**
* This file is part of MMM.
*
* MMM is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "RigidBodyConverter.h"
#include "RigidBodyConverterFactory.h"

#include <boost/extension/shared_library.hpp>
#include <boost/function.hpp>

namespace MMM {

    ConverterFactory::SubClassRegistry RigidBodyConverterFactory::registry(RigidBodyConverterFactory::getName(), &RigidBodyConverterFactory::createInstance);

    RigidBodyConverterFactory::RigidBodyConverterFactory()
        : ConverterFactory()
    {

    }
    RigidBodyConverterFactory::~RigidBodyConverterFactory()
    {

    }

    ConverterPtr RigidBodyConverterFactory::createConverter()
    {
        ConverterPtr converter(new RigidBodyConverter(getName()));
        return converter;
    }

    std::string RigidBodyConverterFactory::getName()
    {
        return "RigidBodyConverterVicon2MMM";
    }

    boost::shared_ptr<ConverterFactory> RigidBodyConverterFactory::createInstance(void*)
    {
        boost::shared_ptr<ConverterFactory> converterFactory(new RigidBodyConverterFactory());
        return converterFactory;
    }

}
#ifdef WIN32
#pragma warning(push)
#pragma warning(disable: 4190) // C-linkage warning can be ignored in our case
#endif

extern "C"
BOOST_EXTENSION_EXPORT_DECL MMM::ConverterFactoryPtr getFactory() {
    MMM::RigidBodyConverterFactoryPtr f(new MMM::RigidBodyConverterFactory());
    return f;
}
#ifdef WIN32
#pragma warning(pop)
#endif


