/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMMTools
* @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __ConverterApplicationHelper_H_
#define __ConverterApplicationHelper_H_

#include <string>
#include <vector>

#include <MMM/Motion/Legacy/Converter/ConverterFactory.h>
#include <MMM/Motion/Legacy/MarkerMotion.h>
#include <MMM/Motion/Legacy/LegacyMotion.h>

// By going through all plugin libraries and initializing them, the ConverterFactories are available for later usage
std::vector<MMM::ConverterFactoryPtr> checkForConverterFactories(const std::vector<std::string>& converterLibSearchPaths);

void setupConverterFactories(const std::vector<std::string>& converterLibSearchPaths);
MMM::ConverterPtr createConverter(const std::string& converterName, const std::string& converterConfigFile);

MMM::ModelPtr readModel(const std::string& modelFile);
MMM::MarkerMotionPtr readMarkerMotion(const std::string& filePath, const std::string& markerPrefix);
MMM::LegacyMotionPtr readMMMMotion(const std::string& filePath);

MMM::ModelProcessorPtr setupModelProcessor(const std::string& modelProcessorName, const std::string& modelProcessorConfigFile);

#endif
