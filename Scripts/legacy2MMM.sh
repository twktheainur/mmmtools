#!/bin/bash
# Script to convert multiple motion files from legacy to new format
#$1 = path to folder with legacy xml files
#$2 = directory 

legacyToMMM()
{
	echo $1
	file=$1
        dir=$2
	file_name=$(basename ${file})
	#mmm_file="mmm.xml"
	#if test ["${file_name}" = "${mmm_file}"]; then
		output_file=${dir}/${file_name}
		echo "Converting ${file} to ${output_file}"
		if ! [[ -f $output_file ]]; then
			../build/bin/MMMLegacyMotionConverter \
				--inputMotion ${file} \
				--outputMotion ${output_file}
		fi
	#fi

}
export -f legacyToMMM

if ! [ -d "$1" ]; then
        echo "Error in required argument 1: $1 is no valid directory!"
	exit
fi
arg1="$1"

if ! [ -d "$2" ]; then
        echo "Error in required argument 2: $2 is no valid directory!"
	exit
fi
arg2="$2"

find $1 -name "*.xml" -type f | xargs -I% -P8 bash -c "legacyToMMM \"%\" \"$arg2\" "
exit
