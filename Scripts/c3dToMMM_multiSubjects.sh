#!/bin/bash
# Script to convert multiple motion files containing different subjects inside from c3d to mmm (only human/subject motion)
#$1 = path to folder with c3d files     		e.g '/common/homes/test/motions/c3d'
#$2 = Marker names in c3d file	seperated by ';'  	e.g '1717;1747'
#$3 = Subject motion names 	seperated by ';'	e.g 'subject1717;subject1747'
#$4 = Config files for subjects seperated by ';'  	e.g '/common/homes/test/motions/configs/modelProcessor1717.xml;/common/homes/test/motions/configs/modelProcessor1747.xml'
#$2, $3, $4 same order is IMPORTANT!!

c3dToMMM()
{
	file=$1
	filename_base="${file%'.c3d'}" #delete .xml from end of string
	../build/bin/MMMC3DConverter \
		--inputMotion ${f} \
		--motionName $3 \
		--markerPrefix $2\
		--outputMotion ${filename_base}.xml
	Configs=(${4//;/ })
	Motions=(${3//;/ })
	Subjects=(${2//;/ })
	for ((i=0;i<${#Subjects[@]};i++))
	do
		../build/bin/MMMMotionConverter \
			--inputMotion ${filename_base}.xml          \
			--motionName ${Motions[i]}                  \
			--converterConfigFile "${PWD}"/../data/NloptConverterVicon2MMM_WinterConfig.xml   \
			--outputModelFile "${PWD}"/../data/Model/Winter/mmm.xml                           \
			--outputModelProcessorConfigFile ${Configs[i]}                                           \
			--outputMotion ${filename_base}.xml
	done
}
export -f c3dToMMM

arg1="$1"
arg2="$2"
arg3="$3"
arg4="$4"
find $1 -name *.c3d -type f | xargs -I% -P8 bash -c "c3dToMMM \"%\" \"$arg2\" \"$arg3\" \"$arg4\""
exit
