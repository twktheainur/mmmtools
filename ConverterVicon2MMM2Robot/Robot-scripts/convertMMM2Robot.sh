#!/bin/bash
if [ ${#2} -eq 0 ]
then
	# Argument 2 not present
	~/home/MMMTools/build/bin/MMMConverterGUI \
        --inputFile $1 \
        --converter NloptConverter \
	--converterConfigFile ConverterMMM2Robot_Config.xml \
	--sourceModel ~/home/MMMTools/data/Model/Winter/mmm.xml \
	--sourceModelProcessor Winter \
	--sourceModelProcessorConfigFile ModelProcessor_Winter.xml \
	--targetModel Robot.xml \
	--targetModelProcessor \"\"
else
	# Argument 2 present
	~/home/MMMTools/build/bin/MMMConverter \
       --inputFile $1 \
        --converter NloptConverter \
	--converterConfigFile ConverterMMM2Robot_Config.xml \
	--sourceModel ~/home/MMMTools/data/Model/Winter/mmm.xml \
	--sourceModelProcessor Winter \
	--sourceModelProcessorConfigFile ModelProcessor_Winter.xml \
	--targetModel Robot.xml \
	--outputFile $2 \
	#--targetModelProcessor \"\"
		
fi
