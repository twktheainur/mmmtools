/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef _MMM_SEGMENTATION_H_
#define _MMM_SEGMENTATION_H_

#include <MMM/MMMCore.h>

namespace MMM
{

enum MMM_IMPORT_EXPORT KeyframeType {
    FIRST, AVERAGE, LAST
};

class MMM_IMPORT_EXPORT Segmentation
{
public:

    Segmentation(float keyTimestep);

    Segmentation(float minTimestep, float maxTimestep, KeyframeType type = KeyframeType::AVERAGE);

    Segmentation(float minTimestep, float maxTimestep, float keyTimestep);

    float getMinTimestep();

    float getMaxTimestep();

    float getKeyTimestep();

    void setKeyTimestep(KeyframeType type);

private:
    float getKeyTimestep(KeyframeType type);

    float minTimestep;
    float maxTimestep;
    float keyTimestep;
};

typedef boost::shared_ptr<Segmentation> SegmentationPtr;
}

#endif // _MMM_SEGMENTATION_H_
