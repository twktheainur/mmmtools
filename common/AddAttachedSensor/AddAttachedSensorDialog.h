/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2018 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_AddAttachedSensorDialog_H_
#define __MMM_AddAttachedSensorDialog_H_

#include <QDialog>
#include <QSpinBox>
#include <QLabel>
#include <QLineEdit>
#include <vector>
#include <tuple>

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#include "AddAttachedSensorConfiguration.h"
#endif

namespace Ui {
class AddAttachedSensorDialog;
}

class AddAttachedSensorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddAttachedSensorDialog(MMM::BaseAddAttachedSensorConfigurationPtr configuration, QWidget* parent = 0);
    ~AddAttachedSensorDialog();

    bool addSensor(MMM::MotionList motions);

private slots:
    void loadFile();
    void setCurrentMotion(int);
    void loadConfigurationFile();
    void saveConfigurationFile();
    void preview();
    void convert();
    void add();

private:
    void error(const std::string &errorMsg);
    Eigen::Matrix4f getTransformationMatrix();
    void loadMotions(MMM::MotionList motions);
    void loadFile(const std::string &dataFilePath);
    void loadConfigurationFile(const std::string &configurationFilePath);

    void setSensorConfiguration();

    MMM::BaseAddAttachedSensorConfigurationPtr configuration;
    MMM::MotionList motions;
    std::string motionName;
    MMM::MotionPtr currentMotion;
    std::string sensorFile;
    bool added;
    std::string configurationFilePath;
    std::string dataFilePath;
    QSettings settings;

    Ui::AddAttachedSensorDialog* ui;

    std::vector<QSpinBox*> indexSpinner;

};

#endif // AddAttachedSensorDialog_H
