#!/bin/bash
# always use absolute Paths
#c3d2mmmDir.sh
#$1 = path to folder with c3d files                                     : 'some path'
#$2 = motion name                                                       : sensorizedGuard
#$3 = Config file                                                       : '/common/homes/students/bretnuetz/You/Config/1747.xml'
#$4 = Model file                                                        :

FILES1=$1/*.c3d


for f in $FILES1
do
    echo ""
    echo "---------MMMC3DConverter------------------"
    echo ""
    filename_base="${f%'.c3d'}" #delete .xml/c3d from end of string
    filename=$(basename -- "$filename_base")
    echo "File:"
    echo ${filename_base}
    echo ${filename}

    echo "Config:"
    echo $3

    echo "Model:"
    echo $4

    echo ""

    /home/fabian/Software/ArmarX/MMMTools/build/bin/MMMC3DConverter \
        --inputMotion ${f} \
        --motionName $2 \
        --markerPrefix $2 \
        --outputMotion ${filename_base}.xml
        

    echo ""
    echo "---------MMMMotionConverter------------------"
    echo ""
    /home/fabian/Software/ArmarX/MMMTools/build/bin/MMMMotionConverter \
        --inputMotion ${filename_base}.xml      \
        --motionName "$2"               \
        --outputModelFile "$4"                    \
        --converterConfigFile "$3"     \
        --outputMotion /home/fabian/Documents/fuerMirkoJeffvonIsabel/Converted/converted_${filename}.xml
done
exit



