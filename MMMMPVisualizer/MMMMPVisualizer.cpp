#include "MMMMPVisualizerConfiguration.h"
#include "MPVisualizer.h"

#include <string>
#include <vector>
#include <tuple>

using namespace MMM;

int main(int argc, char *argv[]) {
    MMM_INFO << " --- MMMMPExporter --- " << endl;
    MMMMPVisualizerConfiguration* configuration = new MMMMPVisualizerConfiguration();
    if (!configuration->processCommandLine(argc, argv)) {
        MMM_ERROR << "Error while processing command line, aborting..." << endl;
        return -1;
    }

    try {
        mplib::representation::VMPType defaultType = mplib::representation::VMPType::PrincipalComponent;
        MPVisualizer* visualizer = new MPVisualizer(configuration->mpFile, configuration->tempMotionFile, defaultType._to_string());
        visualizer->generateAndExportMotion(configuration->nodeSetName, configuration->numSamples, configuration->timeDuration, configuration->outputFileName);
    } catch (MMM::Exception::MMMException& e) {
        MMM_ERROR << e.what() << endl;
        return -2;
    }
}
