#include "MPLearner.h"

#include <mplib/math/canonical_system/CanonicalSystem.h>

void MMM::MPLearner::learnMP(std::shared_ptr<mplib::representation::AbstractMovementPrimitive> mpPtr,
                             MMM::MotionPtr motion,
                             const std::string &nodeSetName,
                             float minTimestep,
                             float maxTimestep,
                             int nsamples)
{
    using namespace mplib::core;

    std::string modefileName = motion->getModel()->getFilename();
    VirtualRobot::RobotPtr robot = VirtualRobot::RobotIO::loadRobot(modefileName);
    VirtualRobot::RobotNodeSetPtr nodeSet = robot->getRobotNodeSet(nodeSetName);
    std::map< std::string, float > jointValueMap = nodeSet->getJointValueMap();

    MMM::KinematicSensorPtr sensor = motion->getSensorByType<MMM::KinematicSensor >(MMM::KinematicSensor::TYPE);
    std::vector<std::string> jointNames = sensor->getJointNames();

    float t0 = sensor->getMinTimestep();
    if (minTimestep >= t0)
        t0 = minTimestep;

    float tT = sensor->getMaxTimestep();
    if (maxTimestep > t0 && maxTimestep < tT)
        tT = maxTimestep;


    std::map<double, DVec> tcpTrajData;
    std::map<double, DVec> jointTrajData;

    VirtualRobot::MathTools::Quaternion oldq;
    for (int i = 0; i <= nsamples; i++)
    {
        float t = t0 + (tT - t0) * (float) i / (float) nsamples;

        MMM::KinematicSensorMeasurementPtr measurment = sensor->getDerivedMeasurement(t);
        Eigen::VectorXf jvs = measurment->getJointAngles();
        for(int j = 0; j < jvs.rows(); ++j)
        {
            std::string jointName = jointNames[j];
            if(nodeSet->hasRobotNode(jointName))
            {
                jointValueMap[jointName] = jvs[j];
            }
        }


        robot->setJointValues(jointValueMap);

        Eigen::Matrix4f tcpPose = nodeSet->getTCP()->getPoseInRootFrame();
        VirtualRobot::MathTools::Quaternion quat = VirtualRobot::MathTools::eigen4f2quat(tcpPose);

        if (i==0)
            oldq = quat;
        else
        {
            float cosHalfTheta = oldq.w * quat.w + oldq.x * quat.x + oldq.y * quat.y + oldq.z * quat.z;
            if(cosHalfTheta < 0)
            {
                quat.w = -quat.w;
                quat.x = -quat.x;
                quat.y = -quat.y;
                quat.z = -quat.z;

            }
            oldq = quat;
        }

        DVec data;
        for(int j = 0; j < 3; ++j)
            data.push_back(tcpPose(j, 3));

        data.push_back(quat.w);
        data.push_back(quat.x);
        data.push_back(quat.y);
        data.push_back(quat.z);
        double ti = (double) i / (double) nsamples;
        tcpTrajData[ti] = data;


        DVec jdata;
        std::vector<float> jointvals = nodeSet->getJointValues();
        for(size_t j = 0; j < jointvals.size(); ++j)
            jdata.push_back((double) jointvals[j]);

        jointTrajData[ti] = jdata;
    }

    SampledTrajectory traj;

    if (mpPtr->getSpace() != mplib::representation::Space::JointSpace)
    {
        traj = SampledTrajectory(tcpTrajData);
    }
    else
    {
        traj = SampledTrajectory(jointTrajData);
    }

    std::vector<SampledTrajectory> trajs{traj};
    mpPtr->learnFromTrajectories(trajs);
}
