#include "MPVisualizerPlugin.h"
#include "MPVisualizerDialog.h"
#include <QFileDialog>

using namespace MMM;

MPVisualizerPlugin::MPVisualizerPlugin(QWidget* widget) :
    MotionHandler(MotionHandlerType::GENERAL, "Visualize MP"),
    searchPath(std::string(MMMTools_SRC_DIR)),
    widget(widget)
{
}

void MPVisualizerPlugin::handleMotion(MotionList motions) {
    if (motions.size() == 0) return;


    MPVisualizerDialog* dialog = new MPVisualizerDialog(motions, widget);

    connect(dialog, SIGNAL(jumpTo(float)), this, SIGNAL(jumpTo(float)));
    connect(dialog, SIGNAL(saveScreenshot(float,std::string,std::string)), this, SIGNAL(saveScreenshot(float,std::string,std::string)));
    connect(dialog, SIGNAL(genMotion(MMM::MotionList)), this, SIGNAL(openMotions(MMM::MotionList)));

    dialog->show();

}

std::string MPVisualizerPlugin::getName() {
    return NAME;
}
