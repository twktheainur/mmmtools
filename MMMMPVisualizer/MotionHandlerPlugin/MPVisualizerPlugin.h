#ifndef __MMM_MPVISUALIZERPLUGIN_H_
#define __MMM_MPVISUALIZERPLUGIN_H_

#include "../../MMMViewer/MotionHandler.h"

namespace MMM
{

class MPVisualizerPlugin : public MotionHandler
{
    Q_OBJECT

public:
    MPVisualizerPlugin(QWidget* widget);

    void handleMotion(MotionList motions);

    virtual std::string getName();

    static constexpr const char* NAME = "MPVisualizer";

private:
    std::string searchPath;
    QWidget* widget;
};

}

#endif
