/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_CSVEXPORTER_H_
#define __MMM_CSVEXPORTER_H_

#include <MMM/Motion/MotionReaderXML.h>

#include <filesystem>

namespace MMM {

class CSVExportPlugin
{
public:
    CSVExportPlugin(const std::vector<std::string> &sensorPluginPaths = std::vector<std::string>()) : motionReader(new MMM::MotionReaderXML(sensorPluginPaths))
    {
    }

    void exportCSV_dir(const std::string &directoryPath, const std::string &exportDirectoryPath, bool localFrame, bool exportHands);

    void exportCSV(const std::string &motionPath, const std::string &exportDirectoryPath, bool localFrame, bool exportHands);

    void exportCSV(MotionList motions, const std::string &exportDirectoryPath, bool localFrame, bool exportHands, float minTimestep, float maxTimestep, float framesPerSecond);

private:
    MMM::MotionReaderXMLPtr motionReader;
};

typedef boost::shared_ptr<CSVExportPlugin> CSVExporterPtr;

}

#endif // __MMM_C3DCONVERTER_H_
