#ifndef __MMM_MPVISUALIZER_H_
#define __MMM_MPVISUALIZER_H_

#include <MMM/Motion/MotionReaderXML.h>
#include <mplib/factories/MPFactoryCreator.h>
#include <mplib/representation/vmp/TaskSpacePrincipalComponentVMP.h>
#include <mplib/representation/vmp/PrincipalComponentVMP.h>

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/MathTools.h>
#include <VirtualRobot/Import/RobotImporterFactory.h>
#include <VirtualRobot/IK/DifferentialIK.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>

#include <filesystem>

namespace MMM {

class MPVisualizer
{
public:
    MPVisualizer(const std::string& mpFile, const std::string& tempMotionFileName, const std::string& mpType);
    MPVisualizer(std::shared_ptr<mplib::representation::AbstractMovementPrimitive> mpPtr, MotionPtr tempMotion):
        mpPtr(mpPtr), tempMotion(tempMotion)
    {}

    MotionPtr getMotion(const std::string& nodeSetName, int nsamples, float minTimeStep = -1, float maxTimeStep = -1);

    void generateAndExportMotion(const std::string &nodeSetName, int nsamples, float maxTimeStep, const std::string& outFile);

    std::shared_ptr<mplib::representation::AbstractMovementPrimitive> getMPPtr(){return mpPtr;}
    MotionPtr getTempMotion(){return tempMotion;}
    void setTempMotion(MotionPtr motion){tempMotion=motion;}
    void setTempMotionFromFile(const std::string& tempFile);

private:
    std::shared_ptr<mplib::representation::AbstractMovementPrimitive> mpPtr;
    MotionPtr tempMotion;
    std::string modelFileName;
    int findKinematicSensorId(KinematicSensorList sensorList, const std::vector<std::string>& nodeSetNames);
};

typedef boost::shared_ptr<MPVisualizer> MPVisualizerPtr;

}

#endif // __MMM_C3DCONVERTER_H_
