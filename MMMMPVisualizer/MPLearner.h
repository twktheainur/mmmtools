#ifndef __MMM_MPLEARNER_H_
#define __MMM_MPLEARNER_H_

#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/MotionWriterXML.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>

#include <mplib/representation/AbstractMovementPrimitive.h>

#include <boost/filesystem.hpp>

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/MathTools.h>
#include <VirtualRobot/Import/RobotImporterFactory.h>

namespace MMM {
class MPLearner
{
public:
    MPLearner()
    {
    }

    void learnMP(std::shared_ptr<mplib::representation::AbstractMovementPrimitive>,
                 MotionPtr motion,
                 const std::string& nodeSetName,
                 float minTimestep=-1,
                 float maxTimestep=-1,
                 int nsamples=1000);

};

typedef boost::shared_ptr<MPLearner> MPLearnerPtr;

}

#endif
