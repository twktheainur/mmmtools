/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_WHOLEBODYDYNAMICCALCULATOR_H_
#define __MMM_WHOLEBODYDYNAMICCALCULATOR_H_

#include <Eigen/StdVector>
#include <MMM/Motion/Motion.h>
#include <MMM/Motion/Plugin/WholeBodyDynamicPlugin/WholeBodyDynamicSensor.h>

namespace MMM
{

struct FrameSegmentData
{
    Eigen::Vector3f position;
    Eigen::Matrix3f rotation;
    Eigen::MatrixXf jacobian;
};

class WholeBodyDynamicCalculator
{
public:
    static void calculate(MotionPtr motion);

private:
    static WholeBodyDynamicSensorPtr calculateWholeBodyDynamicSensor(MotionPtr motion);
};

}

#endif // __MMM_WHOLEBODYDYNAMICCALCULATOR_H_
