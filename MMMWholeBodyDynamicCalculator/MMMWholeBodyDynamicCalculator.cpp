/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <VirtualRobot/RuntimeEnvironment.h>
#include <MMM/Motion/Motion.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/MotionWriterXML.h>
#include "MMMWholeBodyDynamicCalculatorConfiguration.h"
#include "WholeBodyDynamicCalculator.h"

#include <string>
#include <map>
#include <vector>
#include <exception>

int main(int argc, char *argv[])
{
    MMM_INFO << " --- MMMWholeBodyDynamicCalculator --- " << std::endl;
    MMMWholeBodyDynamicCalculatorConfiguration *configuration = new MMMWholeBodyDynamicCalculatorConfiguration();
    if (!configuration->processCommandLine(argc, argv))
    {
        MMM_ERROR << "Error while processing command line, aborting..." << std::endl;
        return -1;
    }

    MMM::MotionList motions;
    try {
        MMM::MotionReaderXMLPtr motionReader(new MMM::MotionReaderXML(configuration->sensorPluginPaths));
        MMM_INFO << "Reading motion file '" << configuration->inputMotionPath << "'!" << std::endl;
        motions = motionReader->loadAllMotions(configuration->inputMotionPath);
    }
    catch (MMM::Exception::MMMException &e) {
        MMM_ERROR << e.what() << std::endl;
        return -2;
    }

    for (MMM::MotionPtr motion : motions) {
        try {
            MMM::WholeBodyDynamicCalculator::calculate(motion);
        }
        catch (MMM::Exception::MMMException &e) {
            MMM_INFO << e.what() << " Ignoring this motion!" << std::endl;
        }
    }

    MMM_INFO << "Writing motions to " << configuration->outputMotionPath << std::endl;
    MMM::MotionWriterXML::writeMotion(motions, configuration->outputMotionPath);

    return 0;
}
