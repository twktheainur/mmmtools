#include "MPExporterFactory.h"
#include "MPExporterPlugin.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry MPExporterFactory::registry(MPExporterPlugin::NAME, &MPExporterFactory::createInstance);

MPExporterFactory::MPExporterFactory() : MotionHandlerFactory() {}

MPExporterFactory::~MPExporterFactory() {}

std::string MPExporterFactory::getName() {
    return MPExporterPlugin::NAME;
}

MotionHandlerPtr MPExporterFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new MPExporterPlugin(widget));
}

MotionHandlerFactoryPtr MPExporterFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new MPExporterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new MPExporterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
