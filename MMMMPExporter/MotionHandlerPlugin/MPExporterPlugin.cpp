#include "MPExporterPlugin.h"
#include "MPExporterDialog.h"
#include <QFileDialog>

using namespace MMM;

MPExporterPlugin::MPExporterPlugin(QWidget* widget) :
    MotionHandler(MotionHandlerType::GENERAL, "Learn and export MP of Motions"),
    searchPath(std::string(MMMTools_SRC_DIR)),
    widget(widget)
{
}

void MPExporterPlugin::handleMotion(MotionList motions) {
    if (motions.size() == 0) return;


    MPExporterDialog* dialog = new MPExporterDialog(motions, widget);

    connect(dialog, SIGNAL(jumpTo(float)), this, SIGNAL(jumpTo(float)));
    connect(dialog, SIGNAL(saveScreenshot(float,std::string,std::string)), this, SIGNAL(saveScreenshot(float,std::string,std::string)));
    dialog->show();

}

std::string MPExporterPlugin::getName() {
    return NAME;
}
