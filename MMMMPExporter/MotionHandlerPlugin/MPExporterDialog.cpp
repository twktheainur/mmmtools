#include "MPExporterDialog.h"
#include "ui_MPExporterDialog.h"
#include "../MPExporter.h"
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <QFileDialog>
#include <QMessageBox>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensorMeasurement.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>

using namespace MMM;

MPExporterDialog::MPExporterDialog(MMM::MotionList motions, QWidget* parent) :
    QDialog(parent),
    ui(new Ui::MPExporterDialog),
    motions(motions)
{
    this->parent = parent;
    ui->setupUi(this);

    std::tuple<float, float> timestepTuple = MMM::Motion::calculateMinMaxTimesteps(motions);
    float minTimestep = std::get<0>(timestepTuple);
    float maxTimestep = std::get<1>(timestepTuple);

    ui->MinTimestepSpin->setMinimum(minTimestep);
    ui->MinTimestepSpin->setMaximum(maxTimestep);
    ui->MinTimestepSpin->setValue(minTimestep);

    ui->MaxTimestepSpin->setMinimum(minTimestep);
    ui->MaxTimestepSpin->setMaximum(maxTimestep);
    ui->MaxTimestepSpin->setValue(maxTimestep);

    getAllAvailableNodeSets();
    connect(ui->ExportButton, SIGNAL(clicked()), this, SLOT(learnAndExportMPs()));
    connect(ui->CancelButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->MinTimestepSpin, SIGNAL(valueChanged(double)), this, SLOT(minTimestepChanged(double)));
}

void MPExporterDialog::learnAndExportMPs() {
    float minTimestep = ui->MinTimestepSpin->value();
    float maxTimestep = ui->MaxTimestepSpin->value();
    int nsamples = ui->NSampleSpinBox->value();
    int kernelSize = ui->KernelSizeSpinBox->value();

    std::string dialog_info = "MP Output Directory";
    std::string outdir = QFileDialog::getExistingDirectory(parent, QString::fromStdString(dialog_info),
                                                                   QString::fromStdString(std::string(MMMTools_SRC_DIR))).toStdString();

    std::string dmpType = "UMITSMP";
    if(ui->JSButton->isChecked())
    {
        dmpType = "UMIDMP";
    }

    std::string nodeSetName = ui->nodeSetBox->currentText().toStdString();
    MPExporterPtr mpExporter(new MPExporter(kernelSize, outdir, dmpType, nsamples));

    std::string outfname = ui->BaseFileNameEdit->text().toStdString();
    mpExporter->exportMP(motions, nodeSetName, outfname, minTimestep, maxTimestep);
    close();
}

void MPExporterDialog::minTimestepChanged(double val)
{
    double maxval = ui->MaxTimestepSpin->value();
    if (val > maxval)
        ui->MaxTimestepSpin->setValue(val+0.1);
}

void MPExporterDialog::getAllAvailableNodeSets()
{
    for(size_t id = 0; id < motions.size(); ++id)
    {
        MMM::MotionPtr motion = motions[id];
        boost::filesystem::path modelFileName(motion->getModel()->getFilename());
        if (modelFileName.filename()!="mmm.xml")
        {
            continue;
        }
        VirtualRobot::RobotPtr robot = VirtualRobot::RobotIO::loadRobot(modelFileName.string());
        std::vector<VirtualRobot::RobotNodeSetPtr > nodeSets = robot->getRobotNodeSets();

        for (size_t i = 0; i < nodeSets.size(); ++i)
        {
            VirtualRobot::RobotNodeSetPtr nodeSet = nodeSets[i];
            ui->nodeSetBox->addItem(QString::fromStdString(nodeSet->getName()));
        }
        boost::filesystem::path p(motion->getOriginFilePath());
        ui->BaseFileNameEdit->setText(QString::fromStdString(p.stem().string()));
    }

}

MPExporterDialog::~MPExporterDialog() {
    delete ui;
}
