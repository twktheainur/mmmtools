#ifndef __MMM_MPEXPORTERDIALOG_H_
#define __MMM_MPEXPORTERDIALOG_H_

#include <QDialog>

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#endif

namespace Ui {
class MPExporterDialog;
}

class MPExporterDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MPExporterDialog(MMM::MotionList motions, QWidget* parent = 0);
    ~MPExporterDialog();

private slots:
    void learnAndExportMPs();
    void minTimestepChanged(double val);

signals:
    void jumpTo(float timestep);
    void saveScreenshot(float timestep, const std::string &directory, const std::string &name = std::string());

private:
    void getAllAvailableNodeSets();

    Ui::MPExporterDialog* ui;
    MMM::MotionList motions;
    QWidget* parent;
};

#endif // __MMM_MPExporterDIALOG_H_
