#ifndef __MMM_MPEXPORTER_H_
#define __MMM_MPEXPORTER_H_

#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/MotionWriterXML.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>

#include <dmp/representation/dmp/umidmp.h>
#include <dmp/representation/dmp/umitsmp.h>
#include <dmp/representation/trajectory.h>

#include <boost/filesystem.hpp>

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/MathTools.h>
#include <VirtualRobot/Import/RobotImporterFactory.h>
#include <dmp/representation/dmpfactory.h>

namespace MMM {
class MPExporter
{
public:
    MPExporter(int kernelSize, const std::string& out_dir, const std::string& dmpType, int nsamples=1000):
        kernelSize(kernelSize),
        out_dir(out_dir),
        dmpType(dmpType),
        nsamples(nsamples)
    {
        dmpFactory.reset(new DMP::DMPFactory());
    }

    void exportMP(MotionList motions, const std::string& nodeSetName, std::string outfname="MP", float minTimestep=-1, float maxTimestep=-1);
    void exportMP_file(const std::string& motion_filepath, const std::string& nodeSetName);
    void exportMP_dir(const std::string& motion_dirpath, const std::string& nodeSetName);

private:
    int kernelSize;
    std::string out_dir;
    std::string dmpType;
    int nsamples;

    DMP::DMPFactoryPtr dmpFactory;

};

typedef boost::shared_ptr<MPExporter> MPExporterPtr;

}

#endif // __MMM_C3DCONVERTER_H_
