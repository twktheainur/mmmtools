#include "MPExporter.h"


void MMM::MPExporter::exportMP(MMM::MotionList motions, const std::string &nodeSetName, std::string outfname, float minTimestep, float maxTimestep)
{
    MMM_INFO << "Output Directory: " << out_dir << std::endl;
    MMM::MotionWriterXMLPtr motionWriter(new MMM::MotionWriterXML());
    for(size_t id = 0; id < motions.size(); ++id)
    {
        MMM::MotionPtr motion = motions[id];
        boost::filesystem::path modelFileName(motion->getModel()->getFilename());
        if (modelFileName.filename()!="mmm.xml")
        {
            continue;
        }

        MMM_INFO << "motion name: " << motion->getName() << endl;

        VirtualRobot::RobotPtr robot = VirtualRobot::RobotIO::loadRobot(modelFileName.string());
        VirtualRobot::RobotNodeSetPtr nodeSet = robot->getRobotNodeSet(nodeSetName);
        std::map< std::string, float > jointValueMap = nodeSet->getJointValueMap();

        std::string motionfile = out_dir + "/" + outfname + "_" + motion->getName() + "_tmp.xml";
        std::string outdmpfile = out_dir + "/"+ outfname + "_" + motion->getName() + "_" + nodeSetName + "_vmp" ;
        std::string outdmpconfig = out_dir + "/"+ outfname + "_" + motion->getName() + "_" + nodeSetName + "_vmp_config" ;

        MMM::KinematicSensorPtr sensor = motion->getSensorByType<MMM::KinematicSensor >(MMM::KinematicSensor::TYPE);
        std::vector<std::string> jointNames = sensor->getJointNames();

        float t0 = sensor->getMinTimestep();
        if (minTimestep >= t0)
            t0 = minTimestep;

        float tT = sensor->getMaxTimestep();
        if (maxTimestep > t0 && maxTimestep < tT)
            tT = maxTimestep;


        std::map<double, DMP::DVec> tcpTrajData;
        std::map<double, DMP::DVec> jointTrajData;

        VirtualRobot::MathTools::Quaternion oldq;
        for (int i = 0; i <= nsamples; i++)
        {
            float t = t0 + (tT - t0) * (float) i / (float) nsamples;

            MMM::KinematicSensorMeasurementPtr measurment = sensor->getDerivedMeasurement(t);
            Eigen::VectorXf jvs = measurment->getJointAngles();
            for(int j = 0; j < jvs.rows(); ++j)
            {
                std::string jointName = jointNames[j];
                if(nodeSet->hasRobotNode(jointName))
                {
                    jointValueMap[jointName] = jvs[j];
                }
            }


            robot->setJointValues(jointValueMap);

            Eigen::Matrix4f tcpPose = nodeSet->getTCP()->getPoseInRootFrame();
            VirtualRobot::MathTools::Quaternion quat = VirtualRobot::MathTools::eigen4f2quat(tcpPose);

            if (i==0)
                oldq = quat;
            else
            {
                float cosHalfTheta = oldq.w * quat.w + oldq.x * quat.x + oldq.y * quat.y + oldq.z * quat.z;
                if(cosHalfTheta < 0)
                {
                    quat.w = -quat.w;
                    quat.x = -quat.x;
                    quat.y = -quat.y;
                    quat.z = -quat.z;
                }
                oldq = quat;
            }

            DMP::DVec data;
            for(int j = 0; j < 3; ++j)
                data.push_back(tcpPose(j, 3));

            data.push_back(quat.w);
            data.push_back(quat.x);
            data.push_back(quat.y);
            data.push_back(quat.z);
            double ti = (double) i / (double) nsamples;
            tcpTrajData[ti] = data;


            DMP::DVec jdata;
            std::vector<float> jointvals = nodeSet->getJointValues();
            for(size_t j = 0; j < jointvals.size(); ++j)
                jdata.push_back((double) jointvals[j]);

            jointTrajData[ti] = jdata;
        }

        motionWriter->writeMotion(motion, motionfile);

        DMP::DMPInterfacePtr dmp = dmpFactory->getDMP(dmpType);
        DMP::SampledTrajectoryV2 traj;

        if (dmp->getDMPType() == "UMITSMP")
        {
            traj = DMP::SampledTrajectoryV2(tcpTrajData);
        }
        else
        {
            traj = DMP::SampledTrajectoryV2(jointTrajData);
        }

        DMP::Vec<DMP::SampledTrajectoryV2 > trajs;
        trajs.push_back(traj);

        dmp->learnFromTrajectories(trajs);
        dmp->save(outdmpfile);

        DMP::MPConfig config(dmp);
        config.motionName = motion->getName();
        config.nodeSetName = nodeSetName;
        config.modelfilename = modelFileName.string();
        config.timeDuration = tT - t0;

        config.save(outdmpconfig);
    }
}

void MMM::MPExporter::exportMP_file(const std::string &motion_filepath, const std::string &nodeSetName)
{
    boost::filesystem::path p(motion_filepath);
    MMM::MotionReaderXMLPtr motionReader(new MMM::MotionReaderXML());
    MMM::MotionList motions = motionReader->loadAllMotions(motion_filepath);
    exportMP(motions, nodeSetName, p.stem().string());
}

void MMM::MPExporter::exportMP_dir(const std::string &motion_dirpath, const std::string &nodeSetName)
{
    using namespace boost::filesystem;
    path p(motion_dirpath);
    if (!is_directory(p))
    {
        MMM_ERROR << p << " is not a directory " << endl;
        return;
    }


    for(auto& entry : boost::make_iterator_range(directory_iterator(p), {}))
    {
        std::string fileName = entry.path().string();
        MMM_INFO << " handling " << fileName << endl;

        exportMP_file(fileName, nodeSetName);
    }
}

