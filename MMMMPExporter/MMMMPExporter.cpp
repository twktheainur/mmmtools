#include "MMMMPExporterConfiguration.h"
#include "MPExporter.h"

#include <string>
#include <vector>
#include <tuple>

using namespace MMM;

int main(int argc, char *argv[]) {
    MMM_INFO << " --- MMMMPExporter --- " << endl;
    MMMMPExporterConfiguration* configuration = new MMMMPExporterConfiguration();
    if (!configuration->processCommandLine(argc, argv)) {
        MMM_ERROR << "Error while processing command line, aborting..." << endl;
        return -1;
    }

    try {
        MPExporter* exporter = new MPExporter(configuration->kernelSize,
                                              configuration->outputDir,
                                              configuration->mpType,
                                              configuration->numSamples);

        if (!configuration->inputMotionPath.empty()) {
            exporter->exportMP_file(configuration->inputMotionPath, configuration->nodeSetName);
        }
        else {
            exporter->exportMP_dir(configuration->inputMotionDir, configuration->nodeSetName);
        }

    } catch (MMM::Exception::MMMException& e) {
        MMM_ERROR << e.what() << endl;
        return -2;
    }
}
