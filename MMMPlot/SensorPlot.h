/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2018 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SENSORPLOT_H_
#define __MMM_SENSORPLOT_H_

#ifndef Q_MOC_RUN
#include <MMM/MMMCore.h>
#include <MMM/MMMImportExport.h>
#include <MMM/Model/Model.h>
#include <MMM/Motion/Motion.h>
#endif

#include <QVector>
#include <Eigen/Core>
#include <string> 
#include <vector>

namespace MMM
{

class MMM_IMPORT_EXPORT SensorPlot
{
public:   
    virtual std::vector<std::string> getNames() = 0;

    virtual std::tuple<QVector<double>, QVector<double> > getPlot(std::string name) = 0;

    virtual std::string getSensorName() = 0;
};

typedef boost::shared_ptr<SensorPlot> SensorPlotPtr;

}

#endif 
