#include "SinglePlotWidget.h"
#include "ui_SinglePlotWidget.h"

#include <QFileDialog>
#include <QInputDialog>
#include <QMenu>

#include "MMM/XMLTools.h"

SinglePlotWidget::SinglePlotWidget(MMM::SensorPlotFactoryPtr plot, const std::string &name, QWidget *parent, bool standalone) :
    QWidget(parent),
    ui(new Ui::SinglePlotWidget),
    plot(plot),
    customPlot(new QCustomPlot(this)),
    currentMotion(nullptr),
    name(name),
    standalone(standalone),
    slider(nullptr),
    dragSlider(false)
{
    ui->setupUi(this);

    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend | QCP::iSelectPlottables);
    customPlot->xAxis->setLabel("timestep");
    customPlot->yAxis->setLabel(QString::fromStdString(plot->getUnit()));
    customPlot->legend->setVisible(true);
    customPlot->plotLayout()->insertRow(0);
    QCPTextElement *title = new QCPTextElement(customPlot, QString::fromStdString(plot->getValueName() + " values over time"), QFont("sans", 17, QFont::Bold));
    customPlot->plotLayout()->addElement(0, 0, title);

    legendTitle = new QCPTextElement(customPlot);
    legendTitle->setLayer(customPlot->legend->layer());
    legendTitle->setText("Legend");
    legendTitle->setFont(QFont("sans", 9, QFont::Bold));
    if (customPlot->legend->hasElement(0, 0))
      customPlot->legend->insertRow(0);
    customPlot->legend->addElement(0, 0, legendTitle);

    ui->ValueWidget->setColumnCount(2);
    ui->ValueWidget->setHeaderLabels({QString("Plot"), QString::fromStdString(plot->getValueName())});

    connect(ui->AddButton, SIGNAL(pressed()), this, SLOT(addPlot()));
    connect(ui->SaveButton, SIGNAL(pressed()), this, SLOT(savePlot()));
    connect(ui->TimestepDelta, SIGNAL(valueChanged(double)), this, SLOT(changeMotionTimestep(double)));

    // connect slot that ties some axis selections together (especially opposite axes):
    connect(customPlot, SIGNAL(selectionChangedByUser()), this, SLOT(selectionChanged()));
    connect(ui->ChooseMotion, SIGNAL(currentIndexChanged(int)), this, SLOT(motionChanged()));

    // connect slots that takes care that when an axis is selected, only that direction can be dragged and zoomed:
    connect(customPlot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress(QMouseEvent*)));
    connect(customPlot, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel()));
    connect(customPlot, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(mouseMove(QMouseEvent*)));
    connect(customPlot, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(mouseRelease(QMouseEvent*)));

    // make bottom and left axes transfer their ranges to top and right axes:
    connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
    connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));

    // connect some interaction slots:
    connect(customPlot, SIGNAL(axisDoubleClick(QCPAxis*,QCPAxis::SelectablePart,QMouseEvent*)), this, SLOT(axisLabelDoubleClick(QCPAxis*,QCPAxis::SelectablePart)));
    connect(customPlot, SIGNAL(legendDoubleClick(QCPLegend*,QCPAbstractLegendItem*,QMouseEvent*)), this, SLOT(legendDoubleClick(QCPLegend*,QCPAbstractLegendItem*)));
    connect(title, SIGNAL(doubleClicked(QMouseEvent*)), this, SLOT(titleDoubleClick(QMouseEvent*)));

    // connect slot that shows a message in the status bar when a graph is clicked:
    connect(customPlot, SIGNAL(plottableClick(QCPAbstractPlottable*,int,QMouseEvent*)), this, SLOT(graphClicked(QCPAbstractPlottable*,int)));

    // setup policy and connect slot for context menu popup:
    customPlot->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(customPlot, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(contextMenuRequest(QPoint)));

    ui->layout->addWidget(customPlot);
}

void SinglePlotWidget::update(std::string motionFilePath, MMM::MotionList motions) {
    std::string motionFileName = MMM::XML::getFileName(motionFilePath);;
    if (this->motions.find(motionFileName) != this->motions.end()) return;
    MMM::MotionList motionsReduced;
    for (MMM::MotionPtr motion : motions) {
        if (motion->getSensorsByType(plot->getType()).size() > 0) motionsReduced.push_back(motion);
    }
    if (motionsReduced.size() > 0) {
        for (MMM::MotionPtr motion : motionsReduced) {
            std::string motionName = motion->getName();
            this->motions[motionFileName][motionName] = motion;
            ui->ChooseMotion->addItem(QString::fromStdString(motionName + " (" + motionFileName + ")"),
                                      QVariant::fromValue(QStringList({QString::fromStdString(motionFileName), QString::fromStdString(motionName)})));
        }
    }
}

void SinglePlotWidget::motionChanged() {
    QStringList list = ui->ChooseMotion->itemData(ui->ChooseMotion->currentIndex()).toStringList();
    currentMotionFileName = list.at(0).toStdString();
    currentMotion = motions[currentMotionFileName][list.at(1).toStdString()];
    MMM::SensorList sensors = currentMotion->getSensorsByType(plot->getType());
    ui->ValueWidget->clear();
    ui->TimestepDelta->setValue(motionTimestepDelta.find(currentMotionFileName) != motionTimestepDelta.end() ? motionTimestepDelta[currentMotionFileName] : 0.0);
    for (MMM::SensorPtr sensor : sensors) {
        MMM::SensorPlotPtr sensorPlot = plot->createSensorPlot(sensor);
        if (sensors.size() > 1) {
            // if more than one sensor is available add the nem to distinguish them
            QTreeWidgetItem* item = new QTreeWidgetItem();
            item->setText(1, QString::fromStdString(sensor->getUniqueName()));
            ui->ValueWidget->addTopLevelItem(item);
        }
        for (std::string name : sensorPlot->getNames()) {
            QTreeWidgetItem* item = new QTreeWidgetItem();
            item->setCheckState(0, Qt::Unchecked);
            for (std::tuple<std::string, std::string> t : values) {
                if (std::get<0>(t) == sensor->getUniqueName() && std::get<1>(t) == name) {
                    item->setCheckState(0, Qt::Checked);
                    break;
                }
            }
            item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
            item->setText(1, QString::fromStdString(name));
            item->setData(2, Qt::UserRole, QVariant::fromValue(QString::fromStdString(sensor->getUniqueName())));
            ui->ValueWidget->addTopLevelItem(item);
        }
    }
    ui->ValueWidget->expandAll();
    ui->ValueWidget->resizeColumnToContents(0);
    ui->ValueWidget->collapseAll();
}

SinglePlotWidget::~SinglePlotWidget() {
    delete ui;
}

void SinglePlotWidget::addPlot() {
    values.clear();
    for (int i = 0; i < ui->ValueWidget->topLevelItemCount(); i++) {
       QTreeWidgetItem* item = ui->ValueWidget->topLevelItem(i);
       if (item->checkState(0) == Qt::Checked) {
          std::string sensorName = item->data(2, Qt::UserRole).toString().toStdString();
          std::string value = item->text(1).toStdString();
          ValueContainer* vc = new ValueContainer(currentMotionFileName, currentMotion->getName(), sensorName, value);
          if (plotted.find(vc) == plotted.end()) {
              MMM::SensorPlotPtr sensorPlot = plot->createSensorPlot(currentMotion->getSensorByName(sensorName));
              std::tuple<QVector<double>, QVector<double> > t = sensorPlot->getPlot(value);
              checkDifferencesToPlotted(vc);
              addGraph(vc, std::get<0>(t), std::get<1>(t), vc->name);
              plotted.insert(vc);
          }
          values.push_back(std::make_tuple(sensorName, value));
       }
    }

    updateText();
}

void SinglePlotWidget::addGraph(ValueContainer* vc, QVector<double> &x, const QVector<double> &y, const std::string &name) {
    QCPGraph* graph = customPlot->addGraph();
    QPen graphPen;
    graphPen.setColor(QColor(rand()%245+10, rand()%245+10, rand()%245+10));
    graphPen.setWidthF(rand()/(double)RAND_MAX*2+1);
    customPlot->graph()->setPen(graphPen);
    graph->setName(QString::fromStdString(name));
    if (motionTimestepDelta.find(vc->motionFileName) != motionTimestepDelta.end()) {
        for (double &timestep : x) {
            timestep += motionTimestepDelta[vc->motionFileName];
        }
    }
    graph->setData(x, y);
    this->graph.push_back(std::make_tuple(vc, graph));

    customPlot->rescaleAxes();
    customPlot->replot();
}

void SinglePlotWidget::savePlot() {
    QString fileName = QFileDialog::getSaveFileName(this, "Save plot", QString::fromStdString(name + ".png"), tr("Image files (*.png)"));
    if (!fileName.isEmpty()) {
        customPlot->savePng(fileName, 1600, 1200);
    }
}

void SinglePlotWidget::changeMotionTimestep(double timestep) {
    std::string motionFileName = ui->ChooseMotion->itemData(ui->ChooseMotion->currentIndex()).toStringList().at(0).toStdString();
    double delta = timestep - ((motionTimestepDelta.find(motionFileName) != motionTimestepDelta.end()) ? motionTimestepDelta[motionFileName] : 0.0);
    motionTimestepDelta[motionFileName] = timestep;
    std::vector<QCPGraph*> graphsToUpdate;
    for (const std::tuple<ValueContainer*, QCPGraph*> &t : graph) {
        if (std::get<0>(t)->motionFileName == motionFileName) graphsToUpdate.push_back(std::get<1>(t));
    }

    updateMotionDeltaVis(graphsToUpdate, delta);
}

void SinglePlotWidget::titleDoubleClick(QMouseEvent* event) {
  Q_UNUSED(event)
  if (QCPTextElement *title = qobject_cast<QCPTextElement*>(sender())) {
    // Set the plot title by double clicking on it
    bool ok;
    QString newTitle = QInputDialog::getText(this, "QCustomPlot example", "New plot title:", QLineEdit::Normal, title->text(), &ok);
    if (ok) {
      title->setText(newTitle);
      customPlot->replot();
    }
  }
}

void SinglePlotWidget::axisLabelDoubleClick(QCPAxis *axis, QCPAxis::SelectablePart part) {
  // Set an axis label by double clicking on it
  if (part == QCPAxis::spAxisLabel) {
    bool ok;
    QString newLabel = QInputDialog::getText(this, "QCustomPlot example", "New axis label:", QLineEdit::Normal, axis->label(), &ok);
    if (ok) {
      axis->setLabel(newLabel);
      customPlot->replot();
    }
  }
}

void SinglePlotWidget::legendDoubleClick(QCPLegend *legend, QCPAbstractLegendItem *item) {
  // Rename a graph by double clicking on its legend item
  Q_UNUSED(legend)
  if (item) {
    QCPPlottableLegendItem *plItem = qobject_cast<QCPPlottableLegendItem*>(item);
    bool ok;
    QString newName = QInputDialog::getText(this, "QCustomPlot example", "New graph name:", QLineEdit::Normal, plItem->plottable()->name(), &ok);
    if (ok) {
      plItem->plottable()->setName(newName);
      customPlot->replot();
    }
  }
}

void SinglePlotWidget::selectionChanged() {
  /*
   normally, axis base line, axis tick labels and axis labels are selectable separately, but we want
   the user only to be able to select the axis as a whole, so we tie the selected states of the tick labels
   and the axis base line together. However, the axis label shall be selectable individually.

   The selection state of the left and right axes shall be synchronized as well as the state of the
   bottom and top axes.

   Further, we want to synchronize the selection of the graphs with the selection state of the respective
   legend item belonging to that graph. So the user can select a graph by either clicking on the graph itself
   or on its legend item.
  */

  // make top and bottom axes be selected synchronously, and handle axis and tick labels as one selectable object:
  if (customPlot->xAxis->selectedParts().testFlag(QCPAxis::spAxis) || customPlot->xAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
      customPlot->xAxis2->selectedParts().testFlag(QCPAxis::spAxis) || customPlot->xAxis2->selectedParts().testFlag(QCPAxis::spTickLabels)) {
    customPlot->xAxis2->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
    customPlot->xAxis->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
  }
  // make left and right axes be selected synchronously, and handle axis and tick labels as one selectable object:
  if (customPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis) || customPlot->yAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
      customPlot->yAxis2->selectedParts().testFlag(QCPAxis::spAxis) || customPlot->yAxis2->selectedParts().testFlag(QCPAxis::spTickLabels)) {
    customPlot->yAxis2->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
    customPlot->yAxis->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
  }

  // synchronize selection of graphs with selection of corresponding legend items:
  for (int i=0; i<customPlot->graphCount(); ++i) {
    QCPGraph *graph = customPlot->graph(i);
    QCPPlottableLegendItem *item = customPlot->legend->itemWithPlottable(graph);
    if (item->selected() || graph->selected()) {
      item->setSelected(true);
      graph->setSelection(QCPDataSelection(graph->data()->dataRange()));
    }
  }
}

void SinglePlotWidget::mousePress(QMouseEvent* event) {
    if (event->button() != Qt::LeftButton) return;

    if (slider && abs(slider->start->pixelPosition().x() - event->localPos().x()) <= 10) {
        dragSlider = true;
        customPlot->axisRect()->setRangeDrag(0);
    }
    else {
        QCPGraph* dragGraph = nullptr;
        dragGraphs.clear();
        for (int i=0; i < customPlot->graphCount(); ++i) {
          QCPGraph* g = customPlot->graph(i);
          if (g->selectTest(event->localPos(), true) <= 5) {
              dragGraph = g;
              break;
          }
        }
        if (dragGraph) {
            for (const std::tuple<ValueContainer*, QCPGraph*> &t : graph) {
                if (std::get<1>(t) == dragGraph) {
                    dragMotionFileName = std::get<0>(t)->motionFileName;
                    break;
                }
            }
            for (const std::tuple<ValueContainer*, QCPGraph*> &t : graph) {
                if (std::get<0>(t)->motionFileName == dragMotionFileName) {
                    dragGraphs.push_back(std::get<1>(t));
                }
            }
            mousePosX = customPlot->xAxis->pixelToCoord(event->pos().x());
            customPlot->axisRect()->setRangeDrag(0);
        } else {
            if (customPlot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
                customPlot->axisRect()->setRangeDrag(customPlot->xAxis->orientation());
            else if (customPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
                customPlot->axisRect()->setRangeDrag(customPlot->yAxis->orientation());
            else customPlot->axisRect()->setRangeDrag(Qt::Horizontal|Qt::Vertical);
        }
    }
}

void SinglePlotWidget::mouseMove(QMouseEvent* event) {
    if (dragSlider) {
        emit jumpTo(customPlot->xAxis->pixelToCoord(event->pos().x()));
    }
    else if (dragGraphs.size() > 0) {
        double newMouseMovePos = customPlot->xAxis->pixelToCoord(event->pos().x());
        double delta = newMouseMovePos - mousePosX;

        if (motionTimestepDelta.find(dragMotionFileName) != motionTimestepDelta.end()) {
            motionTimestepDelta[dragMotionFileName] += delta;
        }
        else motionTimestepDelta[dragMotionFileName] = delta;
        if (ui->ChooseMotion->itemData(ui->ChooseMotion->currentIndex()).toStringList().at(0).toStdString() == dragMotionFileName) {
            ui->TimestepDelta->setValue(motionTimestepDelta[dragMotionFileName]);
        }

        updateMotionDeltaVis(dragGraphs, delta);
        mousePosX = newMouseMovePos;
    }
}

void SinglePlotWidget::updateMotionDeltaVis(std::vector<QCPGraph*> graphs, double delta) {
    for (auto g : graphs) {
        QVector<double> key, value;
        for (int i = 0; i < g->dataCount(); i++) {
            key.push_back(g->dataMainKey(i) + delta);
            value.push_back(g->dataMainValue(i));
        }
        g->setData(key, value);
        customPlot->replot();
        //customPlot->rescaleAxes();
    }
}

void SinglePlotWidget::mouseRelease(QMouseEvent* event) {
    dragGraphs.clear();
    dragSlider = false;
}

void SinglePlotWidget::mouseWheel() {
  // if an axis is selected, only allow the direction of that axis to be zoomed
  // if no axis is selected, both directions may be zoomed
  if (customPlot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
    customPlot->axisRect()->setRangeZoom(customPlot->xAxis->orientation());
  else if (customPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
    customPlot->axisRect()->setRangeZoom(customPlot->yAxis->orientation());
  else
    customPlot->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
}

void SinglePlotWidget::removeSelectedGraph() {
  if (customPlot->selectedGraphs().size() > 0) {
    QCPGraph* graph = customPlot->selectedGraphs().first();
    ValueContainer* vc;
    for (auto it = this->graph.begin(); it != this->graph.end(); it++) {
        if (std::get<1>(*it) == graph) {
            vc = std::get<0>(*it);
            this->graph.erase(it);
            break;
        }
    }
    plotted.erase(plotted.find(vc));
    for (auto it = this->values.begin(); it != this->values.end(); it++) {
        if (std::get<0>(*it) == vc->sensorName && std::get<1>(*it) == vc->valueName) {
            this->values.erase(it);
            break;
        }
    }
    customPlot->removeGraph(graph);
    updateText();
  }
}

void SinglePlotWidget::removeAllGraphs() {
  customPlot->clearGraphs();
  plotted.clear();
  graph.clear();
  values.clear();
  updateText();
}

void SinglePlotWidget::contextMenuRequest(QPoint pos) {
  QMenu *menu = new QMenu(this);
  menu->setAttribute(Qt::WA_DeleteOnClose);

  if (customPlot->legend->selectTest(pos, false) >= 0) // context menu on legend requested
  {
    menu->addAction("Move to top left", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignLeft));
    menu->addAction("Move to top center", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignHCenter));
    menu->addAction("Move to top right", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignRight));
    menu->addAction("Move to bottom right", this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignRight));
    menu->addAction("Move to bottom left", this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignLeft));
  } else {
    if (!standalone) {
        if (customPlot->selectedGraphs().size() > 0) {
            menu->addAction(QString::fromStdString("Open motion of selected graph"), this, SLOT(openSelectedGraphMotion()));
        }
        if (customPlot->graphCount() > 0) {
            mousePosX = customPlot->xAxis->pixelToCoord(pos.x());
            menu->addAction(QString::fromStdString("Open all plotted motions"), this, SLOT(openPlottedMotions()));
            menu->addAction(QString::fromStdString("Jump to timestep " + std::to_string(mousePosX)), this, SLOT(jumpToTimestep()));
        }
        if (menu->children().size() > 0) menu->addSeparator();
    }
    if (customPlot->selectedGraphs().size() > 0)
      menu->addAction("Remove selected graph", this, SLOT(removeSelectedGraph()));
    if (customPlot->graphCount() > 0)
      menu->addAction("Remove all graphs", this, SLOT(removeAllGraphs()));
  }

  menu->popup(customPlot->mapToGlobal(pos));
}

void SinglePlotWidget::moveLegend() {
  if (QAction* contextAction = qobject_cast<QAction*>(sender())) {
    bool ok;
    int dataInt = contextAction->data().toInt(&ok);
    if (ok) {
      customPlot->axisRect()->insetLayout()->setInsetAlignment(0, (Qt::Alignment)dataInt);
      customPlot->replot();
    }
  }
}

void SinglePlotWidget::graphClicked(QCPAbstractPlottable *plottable, int dataIndex) {
  // since we know we only have QCPGraphs in the plot, we can immediately access interface1D()
  // usually it's better to first check whether interface1D() returns non-zero, and only then use it.
  double dataValue = plottable->interface1D()->dataMainValue(dataIndex);
  double dataKey = plottable->interface1D()->dataMainKey(dataIndex);
  QString message = QString("Clicked on graph '%1' at timestep %2 with value %3.").arg(plottable->name()).arg(dataKey).arg(dataValue);
  emit showMessage(message.toStdString());
}

void SinglePlotWidget::checkDifferencesToPlotted(ValueContainer* vc) {
    if (plotted.size() == 0) return;
    bool x1 = (*plotted.begin())->x1;
    bool x2 = (*plotted.begin())->x2;
    bool x3 = (*plotted.begin())->x3;
    bool x4 = (*plotted.begin())->x4;
    for (ValueContainer* vc_other : plotted) {
        if (vc_other->motionFileName != vc->motionFileName) x1 = true;
        if (vc_other->motionName != vc->motionName) x2 = true;
        if (vc_other->sensorName != vc->sensorName) x3 = true;
        if (vc_other->valueName != vc->valueName) x4 = true;
    }

    for (ValueContainer* vc_other : plotted) {
        vc_other->setName(x1, x2, x3, x4);
    }
    vc->setName(x1, x2, x3, x4);
}

void SinglePlotWidget::updateText() {
    if (this->graph.size() == 0) {
        legendTitle->setText(QString::fromStdString("Legend"));
    } else {
        legendTitle->setText(QString::fromStdString((std::get<0>(*this->graph.begin()))->getHeader()));
        for (const std::tuple<ValueContainer*, QCPGraph*> &t : this->graph) {
            std::get<1>(t)->setName(QString::fromStdString(std::get<0>(t)->name));
        }
        customPlot->replot();
    }
}

void SinglePlotWidget::openSelectedGraphMotion() {
    if (customPlot->selectedGraphs().size() > 0) {
      QCPGraph* graph = customPlot->selectedGraphs().first();
      for (const auto &t : this->graph) {
          if (std::get<1>(t) == graph) {
              MMM::MotionList motions;
              MMM::MotionPtr motion = this->motions[std::get<0>(t)->motionFileName][std::get<0>(t)->motionName]->clone();
              if (motionTimestepDelta.find(std::get<0>(t)->motionFileName) != motionTimestepDelta.end()) {
                  double timestepDelta = motionTimestepDelta[std::get<0>(t)->motionFileName];
                  for (const auto &sensorData : motion->getSensorData()) {
                      sensorData.second->shiftMeasurements(timestepDelta);
                  }
                  motions.push_back(motion);
              }
              else motions.push_back(motion);
              emit openMotions(motions);
              std::set<std::string> ignoreSensorNames = motion->getSensorTypes();
              ignoreSensorNames.erase(ignoreSensorNames.find(plot->getType()));
              emit jumpTo(motion->getCommonMinTimestep(ignoreSensorNames));
          }
      }
    }
}

void SinglePlotWidget::openPlottedMotions() {
    if (values.size() > 0) {
        std::set<std::tuple<std::string, std::string> > values;
        std::set<std::string> motionNames;
        MMM::MotionList motions;
        for (ValueContainer* vc : plotted) {
            std::tuple<std::string, std::string> tuple = std::make_tuple(vc->motionFileName, vc->motionName);
            if (values.find(tuple) == values.end()) {
                MMM::MotionPtr motion = this->motions[vc->motionFileName][vc->motionName]->clone();
                if (motionNames.find(vc->motionName) != motionNames.end())
                    motion->setName(vc->motionName + " (" + vc->motionFileName + ")");
                if (motionTimestepDelta.find(vc->motionFileName) != motionTimestepDelta.end()) {
                    double timestepDelta = motionTimestepDelta[vc->motionFileName];
                    for (const auto &sensorData : motion->getSensorData()) {
                        sensorData.second->shiftMeasurements(timestepDelta);
                    }
                }
                motions.push_back(motion);
                values.insert(tuple);
                motionNames.insert(vc->motionName);
            }
        }
        emit openMotions(motions);

        float maxMinTimestep = -std::numeric_limits<float>::infinity();
        for (MMM::MotionPtr motion : motions) {
            std::set<std::string> ignoreSensorNames = motion->getSensorTypes();
            ignoreSensorNames.erase(ignoreSensorNames.find(plot->getType()));
            float timestep = motion->getCommonMinTimestep(ignoreSensorNames);
            if (maxMinTimestep < timestep) maxMinTimestep = timestep;
        }
        emit jumpTo(maxMinTimestep);
    }
}

void SinglePlotWidget::jumpToTimestep() {
    if (customPlot->graphCount() > 0) emit jumpTo(mousePosX);
}

MMM::SensorPlotFactoryPtr SinglePlotWidget::getPlotFactory() {
    return plot;
}

void SinglePlotWidget::moveSlider(float timestep) {
    if (customPlot->graphCount() > 0) {
        if (!slider) {
            slider = new QCPItemLine(customPlot);
            slider->setPen(QPen(Qt::red));
            slider->start->setType(QCPItemPosition::ptPlotCoords);
            slider->end->setType(QCPItemPosition::ptPlotCoords);
        }
        slider->start->setCoords(timestep, customPlot->yAxis->range().lower);
        slider->end->setCoords(timestep, customPlot->yAxis->range().upper);
        customPlot->replot();
    }
}
