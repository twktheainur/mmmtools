#include "SensorPlotHandler.h"
#include <QFileDialog>

using namespace MMM;

SensorPlotHandler::SensorPlotHandler(QWidget* widget) :
    MotionHandler(MotionHandlerType::GENERAL, "Plot sensor data", "Ctrl+P"),
    widget(widget),
    dialog(new SensorPlotHandlerDialog())
{
    connect(dialog, SIGNAL(openMotions(MMM::MotionList)), this, SIGNAL(openMotions(MMM::MotionList)));
    connect(dialog, SIGNAL(jumpTo(float)), this, SIGNAL(jumpTo(float)));
}

void SensorPlotHandler::handleMotion(MotionList motions) {
    dialog->loadMotions(motions);
    dialog->show();
}

std::string SensorPlotHandler::getName() {
    return NAME;
}

void SensorPlotHandler::addImportMotionHandler(MotionHandlerPtr motionHandler) {
    dialog->addImportMotionHandler(motionHandler);
}

boost::shared_ptr<IPluginHandler> SensorPlotHandler::getPluginHandler() {
    boost::shared_ptr<PluginHandler<MMM::SensorPlotFactory> > pluginHandler(new PluginHandler<MMM::SensorPlotFactory>(getPluginHandlerID(), SENSOR_PLOT_PLUGIN_LIB_DIR));
    pluginHandler->updateSignal.connect(boost::bind(&PlotWidget::update, dialog->getPlotWidget(), _1));
    pluginHandler->emitUpdate();
    return pluginHandler;
}

std::string SensorPlotHandler::getPluginHandlerID() {
    return "Sensor Plotter";
}

void SensorPlotHandler::timestepChanged(float timestep) {
    if (dialog->isVisible()) dialog->moveSlider(timestep);
}
