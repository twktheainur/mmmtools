/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2018 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SENSORPLOTHANDLERDIALOG_H_
#define __MMM_SENSORPLOTHANDLERDIALOG_H_

#include <QDialog>

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#include "../../MMMViewer/MotionHandler.h"
#include "../PlotWidget.h"
#include <QSignalMapper>
#include <QStatusBar>
#endif

namespace Ui {
class SensorPlotHandlerDialog;
}

class SensorPlotHandlerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SensorPlotHandlerDialog(QWidget* parent = 0);
    ~SensorPlotHandlerDialog();
    void addImportMotionHandler(MMM::MotionHandlerPtr motionHandler);
    void update(std::map<std::string, MMM::SensorPlotFactoryPtr> sensorPlotFactories);
    PlotWidget* getPlotWidget();
    void moveSlider(float timestep);

signals:
    void openMotions(MMM::MotionList motions);
    void jumpTo(float timestep);

public slots:
    void loadMotions(MMM::MotionList motions);

private slots:
    void handleMotion(const QString &name);
    void showMessage(const std::string &message);

private:
    Ui::SensorPlotHandlerDialog* ui;
    PlotWidget* plotWidget;
    QSignalMapper* signalMapper;
    std::map<std::string, MMM::MotionHandlerPtr> importMotionHandler;
    QStatusBar* statusBar;
};

#endif // __MMM_SensorPlotHandlerDIALOG_H_
