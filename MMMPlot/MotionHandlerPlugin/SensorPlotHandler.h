/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2018 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SENSORPLOTHANDLER_H_
#define __MMM_SENSORPLOTHANDLER_H_

#ifndef Q_MOC_RUN
#include "../../MMMViewer/MotionHandler.h"
#endif

#include "SensorPlotHandlerDialog.h"

namespace MMM
{

class SensorPlotHandler : public MotionHandler
{
    Q_OBJECT

public:
    SensorPlotHandler(QWidget* widget);

    void handleMotion(MotionList motions);

    virtual std::string getName();

    void addImportMotionHandler(MotionHandlerPtr motionHandler);

    boost::shared_ptr<IPluginHandler> getPluginHandler();

    std::string getPluginHandlerID();

    static constexpr const char* NAME = "SensorPlotHandler";

public slots:
    void timestepChanged(float timestep);

private:
    QWidget* widget;
    SensorPlotHandlerDialog* dialog;
};

}

#endif
