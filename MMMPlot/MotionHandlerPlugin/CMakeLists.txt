project(SensorPlotHandler)

set(SensorPlotHandler_Sources
    SensorPlotHandlerFactory.cpp
    SensorPlotHandler.cpp
    SensorPlotHandlerDialog.cpp
    ../PlotWidget.cpp
    ../SinglePlotWidget.cpp
    ../qcustomplot.cpp
    ../../common/HandleMotionsWithoutModel.cpp
)

set(SensorPlotHandler_Headers
    SensorPlotHandlerFactory.h
    SensorPlotHandler.h
    SensorPlotHandlerDialog.h
    ../PlotWidget.h
    ../SensorPlotFactory.h
    ../SinglePlotWidget.h
    ../qcustomplot.h
    ../SensorPlot.h
    ../../common/HandleMotionsWithoutModel.h
)

set(SensorPlotHandler_Moc
    SensorPlotHandlerDialog.h
    SensorPlotHandler.h
    ../../MMMViewer/MotionHandler.h
    ../PlotWidget.h
    ../SinglePlotWidget.h
    ../qcustomplot.h
)

set(SensorPlotHandler_Ui
    SensorPlotHandlerDialog.ui
    ../PlotWidget.ui
    ../SinglePlotWidget.ui
)

DefineMotionHandlerPlugin(${PROJECT_NAME} "${SensorPlotHandler_Sources}" "${SensorPlotHandler_Headers}" "${SensorPlotHandler_Moc}" "${SensorPlotHandler_Ui}" "")
