/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <MMM/Motion/Motion.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>

#include "MMMPlotConfiguration.h"
#include "MainWindow.h"

#include <string>
#include <vector>
#include <tuple>
#include <QVector>
#include <QApplication>

using namespace MMM;

int main(int argc, char *argv[]) {
    MMM_INFO << " --- MMMPlot --- " << endl;
    MMMPlotConfiguration *configuration = new MMMPlotConfiguration();
    if (!configuration->processCommandLine(argc, argv)) {
        MMM_ERROR << "Error while processing command line, aborting..." << std::endl;
        return -1;
    }

    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("H2T");
    QCoreApplication::setOrganizationDomain("h2t.anthropomatik.kit.edu");
    QCoreApplication::setApplicationName("MMMPlot");
    MainWindow* main(new MainWindow(configuration->inputMotionPaths, configuration->plotPluginPaths));

    main->show();

    return app.exec();
}
