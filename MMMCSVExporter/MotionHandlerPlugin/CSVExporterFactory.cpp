#include "CSVExporterFactory.h"
#include "CSVExporterPlugin.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry CSVExporterFactory::registry(CSVExportPlugin::NAME, &CSVExporterFactory::createInstance);

CSVExporterFactory::CSVExporterFactory() : MotionHandlerFactory() {}

CSVExporterFactory::~CSVExporterFactory() {}

std::string CSVExporterFactory::getName() {
    return CSVExportPlugin::NAME;
}

MotionHandlerPtr CSVExporterFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new CSVExportPlugin(widget));
}

MotionHandlerFactoryPtr CSVExporterFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new CSVExporterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new CSVExporterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
