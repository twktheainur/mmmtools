/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/


#ifndef __MMM_SYNCHRONIZEMOTIONHANDLERDIALOG_H_
#define __MMM_SYNCHRONIZEMOTIONHANDLERDIALOG_H_

#include <QDialog>

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#endif

namespace Ui {
class SynchronizeMotionHandlerDialog;
}

class SynchronizeMotionHandlerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SynchronizeMotionHandlerDialog(QWidget* parent, MMM::MotionList motions);
    ~SynchronizeMotionHandlerDialog();

    bool synchronizeMotion();

private slots:
    void setCurrentMotion(int index);
    void chooseMotionToggled(bool checked);
    void allMotionToggled(bool checked);
    void synchronize();

private:
    void loadMotions();

    Ui::SynchronizeMotionHandlerDialog* ui;
    MMM::MotionList motions;
    MMM::MotionPtr currentMotion;
    bool allMotion;
    bool synchronized;
};

#endif // __MMM_SYNCHRONIZEMOTIONHANDLERDIALOG_H_
