#include "AddForce1DHandlerFactory.h"
#include "AddForce1DHandler.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry AddForce1DHandlerFactory::registry(AddForce1DHandler::NAME, &AddForce1DHandlerFactory::createInstance);

AddForce1DHandlerFactory::AddForce1DHandlerFactory() : MotionHandlerFactory() {}

AddForce1DHandlerFactory::~AddForce1DHandlerFactory() {}

std::string AddForce1DHandlerFactory::getName() {
    return AddForce1DHandler::NAME;
}

MotionHandlerPtr AddForce1DHandlerFactory::createMotionHandler(QWidget* parent) {
    return MotionHandlerPtr(new AddForce1DHandler(parent));
}

MotionHandlerFactoryPtr AddForce1DHandlerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new AddForce1DHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new AddForce1DHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
