#include "AddForce1DHandler.h"
#include "../AddForce1DHandlerSensorConfiguration.h"

#include <QFileDialog>
#include <QMessageBox>

using namespace MMM;

AddForce1DHandler::AddForce1DHandler(QWidget* parent) :
    MotionHandler(MotionHandlerType::ADD_SENSOR, "Add 1D force sensor"),
    dialog(0),
    widget(parent)
{
}

void AddForce1DHandler::handleMotion(MotionList motions) {
    if (!dialog) dialog = new AddAttachedSensorDialog(AddForce1DHandlerSensorConfigurationPtr(new AddForce1DHandlerSensorConfiguration()), widget);
    if (motions.size() > 0) {
        MMM::MotionList calculatableMotions;
        for (MMM::MotionPtr motion : motions) {
            if (!motion->getModel()) {
                MMM_INFO << "Ignoring motion '" + motion->getName() + "'! Because the motion has no model." << std::endl;
                continue;
            }
            calculatableMotions.push_back(motion);
        }
        if (calculatableMotions.size() == 0) {
            QMessageBox* msgBox = new QMessageBox(widget);
            msgBox->setText("No calculatable motions found!");
            msgBox->exec();
        } else {
            if (dialog->addSensor(calculatableMotions)) emit openMotions(motions);
        }
    }
    else MMM_ERROR << "Cannot open add Force1D sensor dialog, because no motions are present!" << std::endl;
}

std::string AddForce1DHandler::getName() {
    return NAME;
}
