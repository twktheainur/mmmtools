#include "AddForce3DHandler.h"
#include "../AddForce3DHandlerSensorConfiguration.h"

#include <QFileDialog>
#include <QMessageBox>

using namespace MMM;

AddForce3DHandler::AddForce3DHandler(QWidget* parent) :
    MotionHandler(MotionHandlerType::ADD_SENSOR, "Add 3D force sensor"),
    dialog(0),
    widget(parent)
{
}

void AddForce3DHandler::handleMotion(MotionList motions) {
    if (!dialog) dialog = new AddAttachedSensorDialog(AddForce3DHandlerSensorConfigurationPtr(new AddForce3DHandlerSensorConfiguration()), widget);
    if (motions.size() > 0) {
        MMM::MotionList calculatableMotions;
        for (MMM::MotionPtr motion : motions) {
            if (!motion->getModel()) {
                MMM_INFO << "Ignoring motion '" + motion->getName() + "'! Because the motion has no model." << std::endl;
                continue;
            }
            calculatableMotions.push_back(motion);
        }
        if (calculatableMotions.size() == 0) {
            QMessageBox* msgBox = new QMessageBox(widget);
            msgBox->setText("No calculatable motions found!");
            msgBox->exec();
        } else {
            if (dialog->addSensor(calculatableMotions)) emit openMotions(motions);
        }
    }
    else MMM_ERROR << "Cannot open add Force3D sensor dialog, because no motions are present!" << std::endl;
}

std::string AddForce3DHandler::getName() {
    return NAME;
}
