#include "ConvertMotionHandler.h"
#include <QFileDialog>

using namespace MMM;

ConvertMotionHandler::ConvertMotionHandler(QWidget* widget) :
    MotionHandler(MotionHandlerType::GENERAL, "Convert", "Ctrl+C"),
    dialog(new ConvertMotionHandlerDialog(widget))
{
}

void ConvertMotionHandler::handleMotion(MotionList motions) {
    if (motions.size() > 0) {
        MotionPtr convertedMotion = dialog->convertMotion(motions);
        if (convertedMotion) emit openMotions(Motion::replaceAddMotion(convertedMotion, motions));
    }
    else MMM_ERROR << "Cannot open convert motion dialog, because no motions are present!" << std::endl;
}

std::string ConvertMotionHandler::getName() {
    return NAME;
}

boost::shared_ptr<IPluginHandler> ConvertMotionHandler::getPluginHandler() {
    boost::shared_ptr<PluginHandler<MMM::MotionConverterFactory> > pluginHandler(new PluginHandler<MMM::MotionConverterFactory>(getPluginHandlerID(), MOTION_CONVERTER_PLUGIN_LIB_DIR));
    pluginHandler->updateSignal.connect(boost::bind(&ConvertMotionHandlerDialog::update, dialog, _1));
    pluginHandler->emitUpdate();
    return pluginHandler;
}

std::string ConvertMotionHandler::getPluginHandlerID() {
    return "Motion Converter";
}
