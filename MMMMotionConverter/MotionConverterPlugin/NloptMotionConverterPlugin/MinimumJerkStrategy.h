/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Mirko Wächter
* @copyright  2019 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef _MMM_MINIMUMJERKSTRATEGY_h_
#define _MMM_MINIMUMJERKSTRATEGY_h_

#include <MMM/MMMCore.h>

#include "ConvertingStrategy.h"

namespace MMM
{

class MMM_IMPORT_EXPORT MinimumJerkStrategy : public ConvertingStrategy
{
public:
    MinimumJerkStrategy(const std::map<float, std::map<std::string, Eigen::Vector3f> > &labeledMarkerData, MotionPtr outputMotion, ModelPtr outputModel, const std::vector<std::string> &joints, const std::map<std::string, std::string> &markerMapping, const std::map<std::string, float> &markerWeights);

    void cancel();

    float getCurrentTimestep();

    void convert();

    MotionPtr getInputMotion() const;
    void setInputMotion(const MotionPtr &value);

    float getOverlapPercent() const;
    void setOverlapPercent(float value);

    int getFrameRange() const;
    void setFrameRange(int value);

protected:
    double objectiveFunction(const std::vector<double> &configuration, std::vector<double> &grad);

private:
    void setOptimizationBounds(nlopt::opt& optimizer) const;
    float overlapPercent = 0.2f;
    int frameRange = 50;
    int currentFrameStart = 0;
    int currentFrameEnd;
    unsigned int dimension;
    MotionPtr inputMotion;
    double lastPrintLoss = -1;
    nlopt::opt optimizer;
    std::vector<float> timesteps;
    std::map<float, std::vector<double>> optimizedFrames;
};

typedef boost::shared_ptr<MinimumJerkStrategy> MinimumJerkStrategyPtr;

}

#endif
